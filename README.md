Poner a correr el contenedor en modo _detach_ y conectarse para ejecutar comandos:

```sh
docker run -d --hostname chefserver.playtrak.com.mx --name chef-server-hugoh --privileged -p 443:443 --tmpfs /tmp --tmpfs /run --tmpfs /run/lock -v /sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/hugo_dhd/test_ecs:master
docker exec -ti chef-server bash
```

Dentro del contenedor finalizar la instalación de chef:

```sh
chef-server-ctl reconfigure --chef-license=accept
chef-server-ctl org-create playtrak "PLAYTRAK Sistemas de Monitoreo" -f playtrak.pem 
chef-server-ctl user-create gitlab-runner gitlab runner infraestructura@playtrak.com.mx p@s5w0rD! -f gitlab-runner.pem
chef-server-ctl org-user-add playtrak gitlab-runner --admin
```
